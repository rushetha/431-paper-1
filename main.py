import timeit, random
import matplotlib.pyplot as plt
from collections import Counter
random.seed(1846578791574390247)


# Used GeeksForGeeks for both algorithms for consistency
# Mergesort provided by : https://www.geeksforgeeks.org/python-program-for-merge-sort/
def merge(arr, l, m, r):
    n1 = m - l + 1
    n2 = r - m

    L = [0] * (n1)
    R = [0] * (n2)

    for i in range(0, n1):
        L[i] = arr[l + i]

    for j in range(0, n2):
        R[j] = arr[m + 1 + j]

    i = 0
    j = 0
    k = l

    while i < n1 and j < n2:
        if L[i] <= R[j]:
            arr[k] = L[i]
            i += 1
        else:
            arr[k] = R[j]
            j += 1
        k += 1

    while i < n1:
        arr[k] = L[i]
        i += 1
        k += 1

    while j < n2:
        arr[k] = R[j]
        j += 1
        k += 1


def mergeSort(arr, l, r):
    if l < r:
        m = l + (r - l) // 2

        mergeSort(arr, l, m)
        mergeSort(arr, m + 1, r)
        merge(arr, l, m, r)


# Insertion sort provided by :
def insertionSort(arr):
    n = len(arr)
    for i in range(1, n):
        key = arr[i]
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key


# THis function takes an array and key to chose how to sort and times it
def time_sort(array, key):
    """
    :param array: The array that's sorted
    :param key: the key to chose how to sort
    :return: the time it took to sort the array
    """
    arr_copy = array[:]
    start_time = timeit.default_timer()

    if key == 'insertion':
        insertionSort(arr_copy)
    elif key == 'merge':
        mergeSort(arr_copy, 0, len(arr_copy) - 1)

    end_time = timeit.default_timer()
    return end_time - start_time


# This lets me try array sizes 1 - 1000
input_sizes = [i for i in range(1, 1_001)]
# The amount of generations
runs = 500
# This is ugly but is all the data stored for plotting
random_sizes,  random_sizes_100 = [], []
insertion_times_random, insertion_times_random_100 = [], []
merge_times_random, merge_times_random_100 = [], []
saved = []

for i in range(runs):
    save = None
    for size in input_sizes:
        test_case = [random.randint(0, 100_000) for i in range(size)]
        insertion_time = time_sort(test_case, "insertion")
        merge_time = time_sort(test_case, "merge")

        # This allows me to store the data of the first 100 runs
        # for graphing however, the graph cannot load if this is larger
        if i <= 50:
            random_sizes.append(size)
            insertion_times_random.append(insertion_time)
            merge_times_random.append(merge_time)

        if size <= 100 and i <= 100:
            random_sizes_100.append(size)
            insertion_times_random_100.append(insertion_time)
            merge_times_random_100.append(merge_time)

        # Saving the size n where merge_time < insertion_time excluding one(should be faster)
        if merge_time < insertion_time and size > 1 and not save:
            save = size

    saved.append(save)
    print(f"Generation: {i+1}")

# Print some useful data
saved_sorted = sorted(saved)
print(f"The avg size n where merge sort becomes faster is {sum(saved)//len(saved)}")
print(f"The median size n where merge sort becomes faster is {saved_sorted[runs//2]}")
print(f"The smallest size n where merge sort becomes faster is {saved_sorted[0]}")
print(f"The largest size n where merge sort becomes faster is {saved_sorted[-1]}")
print(f"The mode size n where merge sort becomes faster is {Counter(saved_sorted).most_common(1)[0][0]}")

# This handles all the plotting for the data
# Most of this plotting code is from online and GPT
# This is the time for each sort of the first 100 generations
plt.figure(figsize=(19.2, 10.8))
plt.scatter(random_sizes, insertion_times_random, label='Insertion Sort', marker='o')
plt.scatter(random_sizes, merge_times_random, label='Merge Sort', marker='s')
plt.title('Sorting Times for Random Arrays')
plt.xlabel('Input Size')
plt.ylabel('Time (seconds)')
plt.legend()
plt.grid(True)
plt.savefig('plot.svg', format='svg')

# This is the time for each sort of the first 100 generations however, it only goes to n = 100
plt.figure(figsize=(19.2, 10.8))
plt.scatter(random_sizes_100, insertion_times_random_100, label='Insertion Sort', marker='o')
plt.scatter(random_sizes_100, merge_times_random_100, label='Merge Sort', marker='s')
plt.title('Sorting Times for Random Arrays (First 100 Elements)')
plt.xlabel('Input Size')
plt.ylabel('Time (seconds)')
plt.legend()
plt.grid(True)
plt.xlim(0, 100)
plt.ylim(0, max(max(insertion_times_random_100), max(merge_times_random_100)) * 1.1)
plt.savefig('plot2.svg', format='svg')

plt.figure(figsize=(19.2, 10.8))
plt.plot([i for i in range(runs)], saved, label='Size when insertion was first slower', marker='d')
plt.title('Line Graph with Single Array')
plt.xlabel('Generations')
plt.ylabel('Size of the array where insertion was first slower')
plt.legend()
plt.grid(True)
plt.xlim(0, runs)
plt.savefig('plot3.svg', format='svg')
